package com.juan.calatayud.palmer.prueba.open.client.redis;

import lombok.RequiredArgsConstructor;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Component;

import java.util.Optional;

@Component
@RequiredArgsConstructor
public class ClientRedis {

    private final StringRedisTemplate redisTemplate;

    public void save(String key, String value) {
        ValueOperations<String, String> opsForValueMap = this.redisTemplate.opsForValue();

        if (Boolean.FALSE.equals(redisTemplate.hasKey(key))) {
            opsForValueMap.set(key, value);
        }
    }

    public Optional<String> getValue(String key) {
        ValueOperations<String, String> opsForValueMap = this.redisTemplate.opsForValue();
        return Optional.ofNullable(opsForValueMap.get(key));
    }

}
