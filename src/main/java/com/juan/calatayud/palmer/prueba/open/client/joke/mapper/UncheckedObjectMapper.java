package com.juan.calatayud.palmer.prueba.open.client.joke.mapper;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.juan.calatayud.palmer.prueba.open.client.joke.response.JokeResponse;

import java.io.IOException;
import java.util.concurrent.CompletionException;

public class UncheckedObjectMapper extends ObjectMapper {

    public static JokeResponse readValue(String content) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            return objectMapper.readValue(content, new TypeReference<>() {
            });
        } catch (IOException ioe) {
            throw new CompletionException(ioe);
        }
    }
}
