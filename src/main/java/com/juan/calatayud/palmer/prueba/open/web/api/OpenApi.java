package com.juan.calatayud.palmer.prueba.open.web.api;

import com.juan.calatayud.palmer.prueba.open.web.request.OpenRequest;
import com.juan.calatayud.palmer.prueba.open.web.response.OpenResponse;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

import java.net.URISyntaxException;

@RequestMapping("/v1")
public interface OpenApi {

    @PostMapping(path = "/joke-request")
    @ResponseBody
    OpenResponse randomJoke(@RequestBody(required = false) OpenRequest request) throws URISyntaxException;

    @GetMapping(path = "/joke/{id}")
    @ResponseBody
    OpenResponse getJoke(@PathVariable String id);
}
