package com.juan.calatayud.palmer.prueba.open.service;

import com.juan.calatayud.palmer.prueba.open.web.request.OpenRequest;
import com.juan.calatayud.palmer.prueba.open.web.response.OpenResponse;

import java.net.URISyntaxException;

public interface OpenService {

    OpenResponse randomJoke(OpenRequest openRequest) throws URISyntaxException;

    OpenResponse getJoke(String id);
}
