package com.juan.calatayud.palmer.prueba.open.web.controller;

import com.juan.calatayud.palmer.prueba.open.service.OpenService;
import com.juan.calatayud.palmer.prueba.open.web.api.OpenApi;
import com.juan.calatayud.palmer.prueba.open.web.request.OpenRequest;
import com.juan.calatayud.palmer.prueba.open.web.response.OpenResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.web.bind.annotation.RestController;

import java.net.URISyntaxException;

@RestController
@RequiredArgsConstructor
public class OpenController implements OpenApi {

    private final OpenService openService;

    @Override
    public OpenResponse randomJoke(OpenRequest request) throws URISyntaxException {
        return openService.randomJoke(request);
    }

    @Override
    public OpenResponse getJoke(String id) {
        return openService.getJoke(id);
    }


}
