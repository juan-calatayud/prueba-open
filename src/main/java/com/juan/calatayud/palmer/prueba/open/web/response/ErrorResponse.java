package com.juan.calatayud.palmer.prueba.open.web.response;

import lombok.Builder;
import lombok.Data;

import java.time.LocalDateTime;

@Data
@Builder
public class ErrorResponse {

    private LocalDateTime localDateTime;

    private String title;

    private String description;

    private int status;

    private String path;
}
