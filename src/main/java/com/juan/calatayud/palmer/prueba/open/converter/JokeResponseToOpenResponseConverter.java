package com.juan.calatayud.palmer.prueba.open.converter;

import com.juan.calatayud.palmer.prueba.open.client.joke.response.JokeResponse;
import com.juan.calatayud.palmer.prueba.open.web.response.OpenResponse;
import org.springframework.core.convert.converter.Converter;
import org.springframework.stereotype.Component;

@Component
public class JokeResponseToOpenResponseConverter implements Converter<JokeResponse, OpenResponse> {

    @Override
    public OpenResponse convert(JokeResponse jokeResponse) {
        return OpenResponse.builder()
                .id(jokeResponse.getId())
                .text(jokeResponse.getValue())
                .build();
    }
}
