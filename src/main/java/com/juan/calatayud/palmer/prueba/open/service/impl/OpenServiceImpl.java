package com.juan.calatayud.palmer.prueba.open.service.impl;

import com.juan.calatayud.palmer.prueba.open.client.joke.ClientJoke;
import com.juan.calatayud.palmer.prueba.open.client.joke.response.JokeResponse;
import com.juan.calatayud.palmer.prueba.open.client.redis.ClientRedis;
import com.juan.calatayud.palmer.prueba.open.converter.JokeResponseToOpenResponseConverter;
import com.juan.calatayud.palmer.prueba.open.handler.exception.DataNotFoundException;
import com.juan.calatayud.palmer.prueba.open.service.OpenService;
import com.juan.calatayud.palmer.prueba.open.util.Constant;
import com.juan.calatayud.palmer.prueba.open.util.Util;
import com.juan.calatayud.palmer.prueba.open.web.request.OpenRequest;
import com.juan.calatayud.palmer.prueba.open.web.response.OpenResponse;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Service;

import java.net.URISyntaxException;
import java.util.Objects;
import java.util.Optional;

@RequiredArgsConstructor
@Service
public class OpenServiceImpl implements OpenService {

    private final ClientJoke clientJoke;

    private final JokeResponseToOpenResponseConverter jokeResponseToOpenResponseConverter;

    private final ClientRedis clientRedis;

    @Override
    public OpenResponse randomJoke(OpenRequest openRequest) throws URISyntaxException {
        if (Objects.isNull(openRequest)) {
            JokeResponse jokeResponse = clientJoke.getRandom();
            clientRedis.save(jokeResponse.getId(), jokeResponse.getValue());
            return jokeResponseToOpenResponseConverter.convert(jokeResponse);
        }
        return OpenResponse.builder().id("").text("").build();
    }

    @Override
    public OpenResponse getJoke(String id) {
        Optional<String> response = clientRedis.getValue(id);
        if (response.isPresent()) {
            return OpenResponse.builder().id(id).text(response.get()).build();
        } else {
            throw new DataNotFoundException(Constant.ERROR_DATA_NOT_FOUND_TITLE, Util.getJokeNotFoundMessage(id));
        }
    }
}
