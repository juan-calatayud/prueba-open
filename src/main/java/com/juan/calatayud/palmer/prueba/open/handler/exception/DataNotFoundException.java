package com.juan.calatayud.palmer.prueba.open.handler.exception;

import lombok.Getter;
import lombok.ToString;

@Getter
@ToString
public class DataNotFoundException extends RuntimeException {

    private final String title;

    private final String description;

    public DataNotFoundException(String title, String description) {
        super(description);
        this.title = title;
        this.description = description;
    }
}
