package com.juan.calatayud.palmer.prueba.open.web.request;

import lombok.Getter;
import lombok.Setter;

@Getter
@Setter
public class OpenRequest {

    private String id;

    private String text;
}
