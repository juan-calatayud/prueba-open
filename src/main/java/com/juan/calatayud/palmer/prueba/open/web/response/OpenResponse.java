package com.juan.calatayud.palmer.prueba.open.web.response;

import lombok.Builder;
import lombok.Data;

@Builder
@Data
public class OpenResponse {

    private String id;

    private String text;
}
