package com.juan.calatayud.palmer.prueba.open.handler;

import com.juan.calatayud.palmer.prueba.open.handler.exception.DataNotFoundException;
import com.juan.calatayud.palmer.prueba.open.web.response.ErrorResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.ResponseStatus;

import javax.servlet.http.HttpServletRequest;
import java.time.LocalDateTime;

@ControllerAdvice
public class CustomExceptionHandler {

    @ExceptionHandler(DataNotFoundException.class)
    @ResponseStatus(HttpStatus.NOT_FOUND)
    @ResponseBody
    public final ErrorResponse handleDataNotFoundException(DataNotFoundException ex, HttpServletRequest request) {

        return ErrorResponse.builder().localDateTime(LocalDateTime.now())
                .title(ex.getTitle())
                .description(ex.getMessage())
                .status(HttpStatus.NOT_FOUND.value())
                .path(request.getRequestURI())
                .build();
    }
}
