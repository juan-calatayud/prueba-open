package com.juan.calatayud.palmer.prueba.open.client.joke.response;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import lombok.Data;

@Data
@JsonIgnoreProperties(ignoreUnknown = true)
public class JokeResponse {

    private String id;

    private String value;
}
