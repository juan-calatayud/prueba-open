package com.juan.calatayud.palmer.prueba.open.util;

public class Util {

    private Util() {}

    public static String getJokeNotFoundMessage(String key) {
        return "Joke with key : " + key + " not fount in redis";
    }
}
