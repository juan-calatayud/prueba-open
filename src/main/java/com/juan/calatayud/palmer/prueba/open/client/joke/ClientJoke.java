package com.juan.calatayud.palmer.prueba.open.client.joke;

import com.juan.calatayud.palmer.prueba.open.client.joke.mapper.UncheckedObjectMapper;
import com.juan.calatayud.palmer.prueba.open.client.joke.response.JokeResponse;
import org.springframework.stereotype.Component;

import java.net.URI;
import java.net.URISyntaxException;
import java.net.http.HttpClient;
import java.net.http.HttpRequest;
import java.net.http.HttpResponse;

@Component
public class ClientJoke {

    private static final String URI_RANDOM = "https://api.chucknorris.io/jokes/random";

    public JokeResponse getRandom() throws URISyntaxException {
        HttpClient client = HttpClient.newHttpClient();

        HttpRequest request = HttpRequest.newBuilder()
                .uri(new URI(URI_RANDOM))
                .GET()
                .build();

        return client.sendAsync(request, HttpResponse.BodyHandlers.ofString())
                .thenApply(HttpResponse::body)
                .thenApply(UncheckedObjectMapper::readValue)
                .join();
    }
}
