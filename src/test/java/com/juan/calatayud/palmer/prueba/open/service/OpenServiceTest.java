package com.juan.calatayud.palmer.prueba.open.service;

import com.juan.calatayud.palmer.prueba.open.client.joke.ClientJoke;
import com.juan.calatayud.palmer.prueba.open.client.joke.response.JokeResponse;
import com.juan.calatayud.palmer.prueba.open.client.redis.ClientRedis;
import com.juan.calatayud.palmer.prueba.open.converter.JokeResponseToOpenResponseConverter;
import com.juan.calatayud.palmer.prueba.open.handler.exception.DataNotFoundException;
import com.juan.calatayud.palmer.prueba.open.service.impl.OpenServiceImpl;
import com.juan.calatayud.palmer.prueba.open.web.request.OpenRequest;
import com.juan.calatayud.palmer.prueba.open.web.response.OpenResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URISyntaxException;
import java.util.Optional;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class OpenServiceTest {

    @Mock
    private ClientJoke clientJoke;

    @Mock
    private ClientRedis clientRedis;

    @Mock
    private JokeResponseToOpenResponseConverter jokeResponseToOpenResponseConverter;

    @InjectMocks
    private OpenServiceImpl openService;

    @Test
    public void randomJokeNoBodyOKTest() throws URISyntaxException {

        JokeResponse jokeResponse = new JokeResponse();
        jokeResponse.setId("123145");
        jokeResponse.setValue("text");
        Mockito.when(clientJoke.getRandom()).thenReturn(jokeResponse);

        OpenResponse openResponse = OpenResponse.builder().id("123145").text("text").build();
        Mockito.when(jokeResponseToOpenResponseConverter.convert(Mockito.any(JokeResponse.class))).thenReturn(openResponse);

        OpenResponse response = openService.randomJoke(null);

        assertNotNull(response);
        assertEquals("123145", response.getId());
        assertEquals("text", response.getText());
    }

    @Test
    public void randomJokeBodyOKTest() throws URISyntaxException {

        OpenRequest request = new OpenRequest();
        request.setId("id");
        request.setText("11");
        OpenResponse response = openService.randomJoke(request);

        assertNotNull(response);
        assertEquals("", response.getId());
        assertEquals("", response.getText());
    }

    @Test
    public void getJokeOkTest() {
        Optional<String> responseRedis = Optional.of("value");
        Mockito.when(clientRedis.getValue(Mockito.anyString())).thenReturn(responseRedis);

        OpenResponse response = openService.getJoke("1234");

        assertNotNull(response);
        assertEquals("1234", response.getId());
        assertEquals("value", response.getText());
    }

    @Test(expected = DataNotFoundException.class)
    public void getJokeNotFoundTest() {

        OpenResponse response = openService.getJoke("1234");

        assertNotNull(response);
        assertEquals("1234", response.getId());
        assertEquals("value", response.getText());
    }

}
