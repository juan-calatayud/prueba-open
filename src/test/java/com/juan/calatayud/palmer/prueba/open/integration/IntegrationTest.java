package com.juan.calatayud.palmer.prueba.open.integration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.juan.calatayud.palmer.prueba.open.web.request.OpenRequest;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;

import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultHandlers.print;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class IntegrationTest {

    @Autowired
    private MockMvc mockMvc;

    @Autowired
    private ObjectMapper mapper;

    @Test
    public void postJokeRandomNoBodyOkTest() throws Exception {

        this.mockMvc.perform(post("/v1/joke-request"))
                .andDo(print()).andExpect(status().isOk())
                .andExpect(status().is2xxSuccessful());
    }

    @Test
    public void postJokeRandomBodyOkTest() throws Exception {

        OpenRequest request = new OpenRequest();
        request.setId("123");
        request.setText("aaas");

        String json = mapper.writeValueAsString(request);

        this.mockMvc.perform(
                post("/v1/joke-request")
                .contentType(MediaType.APPLICATION_JSON)
                .content(json)
        ).andDo(print()).andExpect(status().isOk()).andExpect(status().is2xxSuccessful());
    }

    @Test
    public void getJokeDataNotFoundTest() throws Exception {

        this.mockMvc.perform(get("/v1/joke/123"))
                .andDo(print()).andExpect(status().is4xxClientError());
    }
}
