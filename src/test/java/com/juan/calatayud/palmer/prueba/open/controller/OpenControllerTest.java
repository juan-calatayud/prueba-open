package com.juan.calatayud.palmer.prueba.open.controller;

import com.juan.calatayud.palmer.prueba.open.handler.exception.DataNotFoundException;
import com.juan.calatayud.palmer.prueba.open.service.OpenService;
import com.juan.calatayud.palmer.prueba.open.web.controller.OpenController;
import com.juan.calatayud.palmer.prueba.open.web.request.OpenRequest;
import com.juan.calatayud.palmer.prueba.open.web.response.OpenResponse;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.mockito.junit.MockitoJUnitRunner;
import org.springframework.boot.test.context.SpringBootTest;

import java.net.URISyntaxException;

import static org.junit.Assert.assertEquals;
import static org.junit.Assert.assertNotNull;

@SpringBootTest
@RunWith(MockitoJUnitRunner.class)
public class OpenControllerTest {

    @Mock
    private OpenService openService;

    @InjectMocks
    private OpenController openController;

    @Test
    public void randomJokeNoBodyOKTest() throws URISyntaxException {

        OpenResponse openResponse = OpenResponse.builder().id("123145").text("text").build();
        Mockito.when(openService.randomJoke(null)).thenReturn(openResponse);

        OpenResponse response = openController.randomJoke(null);

        assertNotNull(response);
        assertEquals("123145", response.getId());
        assertEquals("text", response.getText());
    }

    @Test
    public void randomJokeBodyOKTest() throws URISyntaxException {

        OpenResponse openResponse = OpenResponse.builder().id("").text("").build();
        Mockito.when(openService.randomJoke(Mockito.any(OpenRequest.class))).thenReturn(openResponse);

        OpenRequest request = new OpenRequest();
        request.setId("id");
        request.setText("11");
        OpenResponse response = openController.randomJoke(request);

        assertNotNull(response);
        assertEquals("", response.getId());
        assertEquals("", response.getText());
    }

    @Test
    public void getJokeOkTest() {
        OpenResponse openResponse = OpenResponse.builder().id("123456").text("text").build();
        Mockito.when(openService.getJoke(Mockito.anyString())).thenReturn(openResponse);

        OpenResponse response = openController.getJoke("123456");

        assertNotNull(response);
        assertEquals("123456", response.getId());
        assertEquals("text", response.getText());

    }

    @Test(expected = DataNotFoundException.class)
    public void getJokeDataNotFoundTest() {
        Mockito.when(openService.getJoke(Mockito.anyString())).thenThrow(DataNotFoundException.class);

        OpenResponse response = openController.getJoke("123456");

        assertNotNull(response);
    }
}
